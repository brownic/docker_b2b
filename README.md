# docker_b2b



## Getting started
Install Docker
```
cat docker_pass.txt | docker login --username lime45 --password-stdin
export SRC_DIR=/path/to/src_dir
docker compose build
docker compose run make_env
```
You should then be presented with a shell. There is currently a simple build script that can be run as a sanity check.
```
cd /root/
export SRC_DIR="/path/to/src/main-display-app-common" && ./build.sh
```
