#!/usr/bin/env bash
#
# A very simple build script. Before running, export SRC_DIR to point to your source
# directory ie:
# export SRC_DIR=/workdir/main-display-app-common && ./build.sh

if [ x"${SRC_DIR}" == "x" ]; then 
  echo SRC_DIR must be set before running this
  exit 1
fi

QMAKE_BIN=/opt/fsl-imx-fb/4.14-sumo/sysroots/cortexa9hf-neon-poky-linux-gnueabi/usr/local/qt5_12_3/bin/qmake
BUILD_DIR=/root/builds/build-premia-CoffeeBeanMachines-Desktop_Qt_5_12_3_GCC_64bit-Debug/

mkdir -p $BUILD_DIR/BunnApp
cd $BUILD_DIR
$QMAKE_BIN -spec devices/linux-imx6-g++ $SRC_DIR/CoffeeBeanMachines.pro
cd $BUILD_DIR/BunnApp/ && $QMAKE_BIN -o Makefile $SRC_DIR/BunnApp/BunnApp.pro -spec devices/linux-imx6-g++ CONFIG+=bunnConfig && /usr/bin/make -j4 -f Makefile 
