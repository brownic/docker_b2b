FROM ubuntu:22.04

RUN mkdir /root/Qt/
RUN mkdir /src
RUN	apt update
RUN apt install -y pkg-config
RUN apt install -y make
COPY /fsl-imx-fb.tar.gz /root/
COPY /qt-5.12.3.tar.gz /root/
COPY /.qt-license /root/
COPY /build.sh /root/
RUN tar -zxvf /root/fsl-imx-fb.tar.gz -C /opt/
RUN tar -zxvf /root/qt-5.12.3.tar.gz -C /root/Qt/
RUN rm -rf /root/*gz
